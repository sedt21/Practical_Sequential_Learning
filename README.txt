*==============================================================================*
PRACTICAL SEQUENTIAL LEARNING
*==============================================================================*
Repository created and Maintained by Austin Gerlt (gerlt.1@osu.edu), but the 
code here represents the work of several others (See HISTORY section below)


OVERVIEW:
Sequential learning is conceptually fairly basic, but actually building up a 
model from scratch is tedious and requires a good deal of computational and 
mathematical background. This repo is a sort of "short cut": it provides a few 
basic pythonic functions to quickly make a model for practical purposes.


FOR QUICK IMPLIMENTATION:
Open up "SL_example.py", and sub in actual data for the "example_data" function.


FOR ACTUALLY UNDERSTANDING WHAT IS HAPPENING:
Read this repo, then read the two main papers in the "References" folder:
	J. Ling et al, "High-Dimensional Materials and ......"
	S. Wager et al, "Confidence Intervals for Random Forests:..."
This is all the math you need to understand what is going on. from there, just 
be aware that forestci.random_forest_error is just a pythonic implimentation of
Wager's infintessimal Jackknife function for pseudo-baysian error.


WHO TO CITE IF USING THIS CODE:
	I personally have no paper on any of this, so feel free to aknowledge me or 
	rope me in to help you as a co-author. however, the following sources should 
	definitely be cited, as they contributed critical code for this:
	
@article{JMLR:v15:wager14a,
  author  = {Stefan Wager and Trevor Hastie and Bradley Efron},
  title   = {Confidence Intervals for Random Forests: The Jackknife and the Infinitesimal Jackknife},
  journal = {Journal of Machine Learning Research},
  year    = {2014},
  volume  = {15},
  number  = {48},
  pages   = {1625-1651},
  url     = {http://jmlr.org/papers/v15/wager14a.html}
}

@article{polimisconfidence,
  title={Confidence Intervals for Random Forests in Python},
  author={Polimis, Kivan and Rokem, Ariel and Hazelton, Bryna},
  journal={Journal of Open Source Software},
  volume={2},
  number={1},
  year={2017}
}

@article{Ling_2017,
   title={High-Dimensional Materials and Process Optimization Using Data-Driven Experimental Design with Well-Calibrated Uncertainty Estimates},
   volume={6},
   ISSN={2193-9772},
   url={http://dx.doi.org/10.1007/s40192-017-0098-z},
   DOI={10.1007/s40192-017-0098-z},
   number={3},
   journal={Integrating Materials and Manufacturing Innovation},
   publisher={Springer Science and Business Media LLC},
   author={Ling, Julia and Hutchinson, Maxwell and Antono, Erin and Paradiso, Sean and Meredig, Bryce},
   year={2017},
   month={Jul},
   pages={207–217}
}

	
BACKGROUND:
This code morphed out of a single day project I did after coming back from the 
Physical Metallurgy GRC in July of 2019. It was used for a proof of concept for
an AFRL proposal, after which it was untouched between 09/2019 and 03/2021. Over
that 18 month period, I more or less forgot much of what i did and why, and am 
now working to backtrace my train of thought and work out how all this code works.
As such, some parts are uncommented or commented incorrectly as of now. Working
to fix this.

HISTORY (roughly):

March 2014:
	Stefan Wagner publishes his paper (in references) which gives a way to get 
	forest uncertainty (basically model variance) from random forests for a 
	given point. code is provided in R. Paper is heavily cited btw, and Stefan
	now works in the statistics department at Standford
April 2017:
	Julia, Maxwell, Erin, Sean, and Bryce publish their paper on using Sequential
	Learning to investigate high dimensional problem spaces in a cost effective 
	and timely manner. also provide 4 case studies on how it can be used. However,
	they don't provide the code, as it is part of the Citrine platform (I believe).
	(This paper, btw, super good tour de force on both what Citrine does and what
	kind of problems SL excels at)
2017 (later?):
	Kivan and Ariel and Bryna from the University of Washington's eScience Institute
	publish a pure python implimentation of Wager's code as part of their 'forestci'
	package. 
July 2019:
	Austin goes to GRC 2019, ask Bryce and Steve Niezgoda a bunch of questions about
	SL, doesn't get the intricate details but understands enough to write some code.
Mon Aug 19 2019:
	I wrote up a toy SL model to show how machines could outpreform domain experts.
	Finished in single sitting with no comments, and didn't save any of the code that
	made any of the images. 
March 2021:
	Began this repo.

